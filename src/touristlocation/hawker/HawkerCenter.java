package touristlocation.hawker;

import touristlocation.TouristLocation;

/**
 * Created by azeemav on 28/4/16.
 */
public class HawkerCenter extends TouristLocation{

    public HawkerCenter(String name, double x, double y) {
        super(name, x, y);
    }
}
