package touristlocation.museum;

import touristlocation.TouristLocation;

/**
 * Created by azeemav on 28/4/16.
 */
public class Museum extends TouristLocation {
    public Museum(String name, double x, double y) {
        super(name, x, y);
    }
}
