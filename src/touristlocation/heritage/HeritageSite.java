package touristlocation.heritage;

import touristlocation.TouristLocation;

/**
 * Created by azeemav on 28/4/16.
 */
public class HeritageSite extends TouristLocation{

    public HeritageSite(String name, double x, double y) {
        super(name, x, y);
    }
}
