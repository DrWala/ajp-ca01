package touristlocation.historic;

import touristlocation.TouristLocation;

/**
 * Created by azeemav on 6/5/16.
 */
public class HistoricSite extends TouristLocation {
    public HistoricSite(String name, double x, double y) {
        super(name, x, y);
    }
}
