package touristlocation;

import globaldata.GlobalDataSet;
import org.apache.commons.codec.language.DoubleMetaphone;

/**
 * Created by azeemav on 18/5/16.
 */
public class TouristLocation {
    public String Name;
    public double X;
    public double Y;

    public TouristLocation(String name, double x, double y) {
        Name = name;
        X = x;
        Y = y;
    }

    public static TouristLocation find(String searchTerm) {
        for (TouristLocation touristLocation : GlobalDataSet.allLocationMap.values()) {
            if (touristLocation.Name.indexOf(searchTerm) != -1) {
                System.out.println("Search result:");
                return touristLocation;
            }
        }
        DoubleMetaphone doubleMetaphone = new DoubleMetaphone();
        String base = doubleMetaphone.doubleMetaphone(searchTerm);
        for (TouristLocation touristLocation : GlobalDataSet.allLocationMap.values()) {
            String foundMetaphone = doubleMetaphone.doubleMetaphone(touristLocation.Name);
            if (foundMetaphone.equals(base)){
                System.out.println("Metaphone result:");
                return touristLocation;
            }
        }
        return null;
    }
}
