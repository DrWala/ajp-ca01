package touristlocation.hotel;

import touristlocation.TouristLocation;

/**
 * Created by azeemav on 28/4/16.
 */
public class Hotel extends TouristLocation {
    public Hotel(String name, double x, double y) {
        super(name, x, y);
    }
}
