package publictransport.bus;

/**
 * Created by azeemav on 2/5/16.
 */
public class PhysicalBusStopForBus extends PhysicalBusStop {
    public int DIR;
    public int ROUTE_SEQ;
    public double DISTANCE;
    public String SVC_NUM;

    public PhysicalBusStopForBus(PhysicalBusStop physicalBusStop) {
        this.BUSCODE = physicalBusStop.BUSCODE;
        this.BUSSTOP_DESC = physicalBusStop.BUSSTOP_DESC;
        this.ROAD_DESCRIPT = physicalBusStop.ROAD_DESCRIPT;
    }
}
