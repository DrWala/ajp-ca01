package publictransport.bus;

import globaldata.GlobalDataSet;

/**
 * Created by azeemav on 12/5/16.
 */
public class NearestBusStop {

    public static PhysicalBusStop get(double x, double y) {
        final int R = 6371; // Radius of the earth
        double lat2, lon2;
        double minDistToBusStop = Double.MAX_VALUE;
        PhysicalBusStop nearestBusStop = null;
        double lat1 = y;
        double lon1 = x;
        for (PhysicalBusStop physicalBusStop : GlobalDataSet.physicalBusStopHashMap.values()) {
            lat2 = physicalBusStop.Y;
            lon2 = physicalBusStop.X;
            double latDistance = toRad(lat2 - lat1);
            double lonDistance = toRad(lon2 - lon1);
            double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
                    Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
                            Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            double distance = R * c;
            if (minDistToBusStop > distance) {
                minDistToBusStop = distance;
                nearestBusStop = physicalBusStop;
            }
        }
        System.out.println("Distance: " + minDistToBusStop);
        System.out.println("MRT: " + nearestBusStop.BUSCODE);
        return nearestBusStop;
    }

    private static double toRad(double value) {
        return value * Math.PI / 180;
    }
}