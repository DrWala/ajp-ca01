package publictransport.bus;

import globaldata.GlobalDataSet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by azeemav on 28/4/16.
 */
public class PhysicalBusStop {

    public float X;
    public float Y;
    public String BUSCODE;
    public String ROAD_DESCRIPT;
    public String BUSSTOP_DESC;
    public boolean visited;
    public HashSet<PhysicalBusStop> Neighbours = new HashSet<>();
    public List<PhysicalBusStop> Path = new ArrayList<>();
    public Set<Bus> BussesAtThisStop = new HashSet<>();

    public PhysicalBusStop() {
    }

    public PhysicalBusStop(String BUSCODE, String ROAD_DESCRIPT, String BUSSTOP_DESC) {
        this.BUSCODE = BUSCODE;
        this.ROAD_DESCRIPT = ROAD_DESCRIPT;
        this.BUSSTOP_DESC = BUSSTOP_DESC;
    }

    public String showPath() {
        Set<Bus> intersection = new HashSet<>(Path.get(0).BussesAtThisStop);
        int nextStart = 0;
        String pathStr = "";
        for (int i = 0; i < Path.size(); i++) {
            if (i > 0) {
                intersection.retainAll(Path.get(i).BussesAtThisStop);
                if (intersection.size() == 0) {
                    intersection = new HashSet<>(Path.get(nextStart).BussesAtThisStop);
                    intersection.retainAll(Path.get(i - 1).BussesAtThisStop);
                    pathStr += "=========TAKE TO GET FROM " + Path.get(nextStart).BUSCODE + " TO " + Path.get(i - 1).BUSCODE + "=========\n";
                    for (Bus bus : intersection) {
                        pathStr += bus.SVC_NUM + "\n";
                    }
                    pathStr += "=================================================\n";
                    intersection = new HashSet<>(Path.get(i).BussesAtThisStop);
                    nextStart = i;
                }
            }
            pathStr += Path.get(i).BUSCODE + " - " + Path.get(i).BUSSTOP_DESC + "\n";
            if (i == Path.size() - 1) {
                pathStr += "=========TAKE TO GET HERE=========\n";
                for (Bus bus : intersection) {
                    pathStr += bus.SVC_NUM + "\n";
                }
                pathStr += "==================================\n";
            }
        }
        return pathStr;
//        for (int i = 0; i < Path.size(); i++) {
//            if (i > 0) {
//                intersection.retainAll(Path.get(i).BussesAtThisStop);
//                if (intersection.size() == 0) {
//                    intersection = new HashSet<>(Path.get(nextStart).BussesAtThisStop);
//                    intersection.retainAll(Path.get(i - 1).BussesAtThisStop);
//                    System.out.println("=========TAKE TO GET FROM " + Path.get(nextStart).BUSCODE + " TO " + Path.get(i - 1).BUSCODE + "=========");
//                    for (Bus bus : intersection) {
//                        System.out.println(bus.SVC_NUM);
//                    }
//                    System.out.println("=================================================");
//                    intersection = new HashSet<>(Path.get(i).BussesAtThisStop);
//                    nextStart = i;
//                }
//            }
//            System.out.println(Path.get(i).BUSCODE + " - " + Path.get(i).BUSSTOP_DESC);
//            if (i == Path.size() - 1) {
//                System.out.println("=========TAKE TO GET HERE=========");
//                for (Bus bus : intersection) {
//                    System.out.println(bus.SVC_NUM);
//                }
//                System.out.println("==================================");
//            }
//        }
    }

    public static PhysicalBusStop find(String searchTerm) {
        return GlobalDataSet.physicalBusStopHashMap.get(searchTerm);
    }

    public static void reset() {
        for (PhysicalBusStop physicalBusStop : GlobalDataSet.physicalBusStopHashMap.values()) {
            physicalBusStop.visited = false;
            physicalBusStop.Path = new ArrayList<>();
        }
    }

}
