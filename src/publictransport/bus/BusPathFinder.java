package publictransport.bus;

import publictransport.mrt.MRTStation;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by azeemav on 10/5/16.
 */
public class BusPathFinder {
    private String destinationStop;

    public PhysicalBusStop findPath(PhysicalBusStop source, PhysicalBusStop destination) {
        destinationStop = destination.BUSCODE;
        PhysicalBusStop destinationBusStop = null;
        if ((destinationBusStop = breadthFirstSearchForShortestPath(source)) != null) {
//            destinationBusStop.showPath();
            return destinationBusStop;
        }
        return null;
    }

    private PhysicalBusStop breadthFirstSearchForShortestPath(PhysicalBusStop source) {
        Queue queue = new LinkedList<MRTStation>();
        queue.add(source);
        boolean isFound = false;
        while (!isFound) {
            PhysicalBusStop firstStopInQueue = (PhysicalBusStop) queue.peek();
            firstStopInQueue.visited = true;
            firstStopInQueue.Path.add(firstStopInQueue);
            if (firstStopInQueue.BUSCODE.equals(destinationStop)) {
                isFound = true;
                return firstStopInQueue;
            } else {
                for (PhysicalBusStop neighbour : firstStopInQueue.Neighbours) {
                    if (!neighbour.visited) {
                        if (neighbour.Path.isEmpty())
                            neighbour.Path.addAll(firstStopInQueue.Path);
                        else {
                            if (neighbour.Path.size() > firstStopInQueue.Path.size()) {
                                neighbour.Path.clear();
                                neighbour.Path.addAll(firstStopInQueue.Path);
                            }
                        }
                        queue.add(neighbour);
                    }
                }
            }
            queue.remove();
        }
        return null;
    }
}
