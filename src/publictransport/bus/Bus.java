package publictransport.bus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by azeemav on 28/4/16.
 */
public class Bus {
    public String SVC_NUM;
    //Bus Stop Code, Bus Stop
//    public Map<String, PhysicalBusStopForBus> dirOne=new LinkedHashMap<>();
//    public Map<String, PhysicalBusStopForBus> dirTwo=new LinkedHashMap<>();
    public List<PhysicalBusStopForBus> dirOne=new ArrayList<>();
    public List<PhysicalBusStopForBus> dirTwo=new ArrayList<>();
}
