package publictransport.mapRoute;

import publictransport.bus.PhysicalBusStop;
import publictransport.mrt.MRTStation;

/**
 * Created by azeemav on 18/5/16.
 */
public class MapRouteCrafter {

    String urlStartString = "https://maps.googleapis.com/maps/api/staticmap?size=650x1000";
    String markerString = "";
    String endString = "&scale=1&key=AIzaSyCiuZ1zuvoFVQc-ZjuiaqZSV0hJkm5JQfs";

    public String mrtRouteCrafter(MRTStation stationForRoute) {
        char count = 'A';
        for (MRTStation path : stationForRoute.Path) {
            double y = path.Y;
            double x = path.X;
            if (x != 0 && y != 0) {
                if (count == 'Z' + 1) {
                    count = '1';
                }
                String markerLine = "&markers=color:red|label:" + count + "|" + y + "," + x;
                markerString += markerLine;
                count++;
            }
            
        }
        return urlStartString + markerString + endString;
    }

    public String busRouteCrafter(PhysicalBusStop stopForRoute) {
        char count = 'A';
        for (PhysicalBusStop path : stopForRoute.Path) {
            double y = path.Y;
            double x = path.X;
            if (x != 0 && y != 0) {
                if (count == 'Z' + 1) {
                    count = '1';
                }
                String markerLine = "&markers=color:red|label:" + count + "|" + y + "," + x;
                markerString += markerLine;
                count++;
            }
            
        }
        return urlStartString + markerString + endString;
    }

}
