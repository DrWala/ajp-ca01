package publictransport.mrt;

import globaldata.GlobalDataSet;

/**
 * Created by azeemav on 12/5/16.
 */
public class NearestMRTStation {
    public NearestMRTStation() {
    }

    public static MRTStation get(double x, double y) {
        final int R = 6371; // Radius of the earth
        double lat2, lon2;
        double minDistToMRT = Double.MAX_VALUE;
        MRTStation nearestMRT = null;
        double lat1 = y;
        double lon1 = x;
        for (MRTStation mrtStation : GlobalDataSet.mrtStationInfoMap.values()) {
            lat2 = mrtStation.Y;
            lon2 = mrtStation.X;
            double latDistance = toRad(lat2 - lat1);
            double lonDistance = toRad(lon2 - lon1);
            double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
                    Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
                            Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            double distance = R * c;
            if (minDistToMRT > distance) {
                minDistToMRT = distance;
                nearestMRT = mrtStation;
            }
        }
        System.out.println("Distance: " + minDistToMRT);
        System.out.println("MRT: " + nearestMRT.MRTStationName);
        return nearestMRT;
    }

    private static double toRad(double value) {
        return value * Math.PI / 180;
    }
}
