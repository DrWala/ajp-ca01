package publictransport.mrt;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by azeemav on 7/5/16.
 */
public class MRTPathFinder {
    private String dest;

    public MRTStation findPath(MRTStation source, MRTStation destination) {
        dest = destination.MRTStationName;
        MRTStation returnedStation = breadthFirstSearchForShortestPath(source);
        if (returnedStation != null) {
//            returnedStation.showPath();
            return returnedStation;
        }
        return null;
    }

    private MRTStation breadthFirstSearchForShortestPath(MRTStation source) {
        Queue queue = new LinkedList<MRTStation>();
        queue.add(source);
        boolean isFound = false;
        while (!isFound) {
            MRTStation firstStationInQueue = (MRTStation) queue.peek();
            firstStationInQueue.Visited = true;
            firstStationInQueue.Path.add(firstStationInQueue);
            if (firstStationInQueue.MRTStationName.equals(dest)) {
                isFound = true;
                return firstStationInQueue;
            } else {
                for (MRTStation neighbour : firstStationInQueue.Neighbours) {
                    if (!neighbour.Visited) {
                        if (neighbour.Path.isEmpty())
                            neighbour.Path.addAll(firstStationInQueue.Path);
                        else {
                            if (neighbour.Path.size() > firstStationInQueue.Path.size()) {
                                neighbour.Path.clear();
                                neighbour.Path.addAll(firstStationInQueue.Path);
                            }
                        }
                        queue.add(neighbour);
                    }
                }
            }
            queue.remove();
        }
        return null;
    }
}
