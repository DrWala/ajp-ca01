package publictransport.mrt;

import globaldata.GlobalDataSet;
import org.apache.commons.codec.language.DoubleMetaphone;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by azeemav on 28/4/16.
 */
public class MRTStation {
    public HashSet<String> MRTCode = new HashSet<>();
    public HashSet<String> LineList = new HashSet<>();
    public HashSet<MRTStation> Neighbours = new HashSet<>();
    public String MRTStationName;
    //    public String Line;
    public float X;
    public float Y;
    public boolean isInterchange = false;
    public boolean Visited = false;
    public int distance = Integer.MAX_VALUE;
    public ArrayList<MRTStation> Path = new ArrayList<>();

    public MRTStation(String MRTCode, String MRTStationName) {
        this.MRTCode.add(MRTCode);
        this.MRTStationName = MRTStationName;
    }

    public void addMrtCode(HashSet<String> MRTCode) {
        this.isInterchange = true;
        this.MRTCode.addAll(MRTCode);
    }

    public void setLineList() {
        for (String mrtCode : MRTCode) {
            LineList.add(mrtCode.substring(0, 2));
        }
    }

    public static void reset() {
        for (MRTStation mrtStation : GlobalDataSet.mrtStationInfoMap.values()) {
            mrtStation.Visited = false;
            mrtStation.Path = new ArrayList<>();
        }
    }

     public String showPath() {
        String pathStr = "";
        pathStr += "Path taken to reach " + MRTStationName + ":\n";
        for (MRTStation path : this.Path) {
            pathStr += path.MRTStationName+"\n";
        }
        return pathStr;
//        System.out.println("\nPath taken to reach " + MRTStationName + ":");
//        for (MRTStation path : this.Path) {
//            System.out.println(path.MRTStationName);
//        }
//        System.out.println("");
    }

    @Override
    public String toString() {
        return MRTStationName;
    }

    public static MRTStation find(String search) {
        for (MRTStation mrtStation : GlobalDataSet.mrtStationInfoMap.values()) {
            if (mrtStation.MRTStationName.indexOf(search) != -1) {
                return mrtStation;
            }
            for (String code : mrtStation.MRTCode) {
                if (code.equals(search))
                    return mrtStation;
            }
        }
        DoubleMetaphone doubleMetaphone = new DoubleMetaphone();
        String base = doubleMetaphone.doubleMetaphone(search);
        for (MRTStation mrtStation : GlobalDataSet.mrtStationInfoMap.values()) {
            String foundMetaphone = doubleMetaphone.doubleMetaphone(mrtStation.MRTStationName);
            if (foundMetaphone.equals(base))
                return mrtStation;
        }
        return null;
    }
}
