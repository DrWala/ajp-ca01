package globaldata;

/**
 * Created by azeemav on 3/5/16.
 */
public enum GlobalDataPath {
    HAWKER("src/globaldata/Hawker.json"),
    HERITAGE("src/globaldata/Heritage.json"),
    HISTORTIC_SITES("src/globaldata/HistoricSites.json"),
    HOTELS("src/globaldata/Hotels.json"),
    BUS_STOP_CODES("src/globaldata/lta-bus_stop_codes.json"),
    BUS_STOP_LOCATIONS("src/globaldata/lta-bus_stop_locations.json"),
    SBS_ROUTE("src/globaldata/lta-sbst_route.json"),
    SMRT_ROUTE("src/globaldata/lta-smrt_route.json"),
    MUSEUMS("src/globaldata/Museums.json"),
    MRT_STATIONS_LOCATIONS("src/globaldata/Stations.json"),
    MRT_STATION_LIST("src/globaldata/MRT.txt");

    private String path;

    GlobalDataPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return path;
    }
}
