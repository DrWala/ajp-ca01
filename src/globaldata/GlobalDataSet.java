package globaldata;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import publictransport.bus.Bus;
import publictransport.bus.PhysicalBusStop;
import publictransport.bus.PhysicalBusStopForBus;
import publictransport.mrt.MRTStation;
import touristlocation.TouristLocation;
import touristlocation.hawker.HawkerCenter;
import touristlocation.heritage.HeritageSite;
import touristlocation.historic.HistoricSite;
import touristlocation.hotel.Hotel;
import touristlocation.museum.Museum;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by azeemav on 28/4/16.
 */
public class GlobalDataSet {

    //Bus Stop Code, Physical Bus Stop
    public static Map<String, PhysicalBusStop> physicalBusStopHashMap = new HashMap<>();
    //Bus Number, Bus
    public static Map<String, Bus> busHashMap = new HashMap<>();
    //Bus Stop Code, BusName, Bus Object
//    public static Map<String, HashMap<String, Bus>> bussesAtBusStopHashMap = new HashMap<>();
    //MRT Station Code OR name, MRTStation
    public static Map<String, MRTStation> mrtStationInfoMap = new ConcurrentHashMap<>();
    //Hawker center name, HawkerCenter
    public static Map<String, HawkerCenter> hawkerCenterMap = new HashMap<>();
    //Heritage site name, HeritageSite
    public static Map<String, HeritageSite> heritageSiteMap = new HashMap<>();
    //Historic site name, HistoricSite
    public static Map<String, HistoricSite> historicSiteMap = new HashMap<>();
    //Hotel name, Hotel
    public static Map<String, Hotel> hotelMap = new HashMap<>();
    //Museum name, Museum
    public static Map<String, Museum> museumMap = new HashMap<>();
    //All map
    public static Map<String, TouristLocation> allLocationMap = new HashMap<>();

    public static void readAllData() throws FileNotFoundException {
        readBusStopData();
        readSBSBusAndRouteData();
        readSMRTBusAndRouteData();
        setBussesAtBusStop();
        setBusStopNeighbours();
        readMrtData();
        setMrtNeighbours();
        readHawkerData();
        readHeritageData();
        readHistoricSiteData();
        readHotelData();
        readMuseumData();
        combine();
    }

    private static void readBusStopData() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonArray jsonArray;
        JsonObject jsonObject;

        //Get all the bus stops
        jsonArray = gson.fromJson(new FileReader(GlobalDataPath.BUS_STOP_CODES.toString()), new TypeToken<JsonArray>() {
        }.getType());
        for (JsonElement jsonElement : jsonArray) {
            jsonObject = jsonElement.getAsJsonObject();
            //Get what's needed from the Json object
            String busStopCode = jsonObject.get("BUS_CODE").getAsString();
            String roadDesc = jsonObject.get("ROAD_DESCRIPT").getAsString();
            String busStopDesc = jsonObject.get("BUSSTOP_DESC").getAsString();
            PhysicalBusStop physicalBusStop = new PhysicalBusStop(busStopCode, roadDesc, busStopDesc);
            physicalBusStopHashMap.put(busStopCode, physicalBusStop);
        }
        jsonArray = gson.fromJson(new FileReader(GlobalDataPath.BUS_STOP_LOCATIONS.toString()), new TypeToken<JsonArray>() {
        }.getType());
        for (JsonElement jsonElement : jsonArray) {
            jsonObject = jsonElement.getAsJsonObject();
            String busStopCode = jsonObject.get("BUSCODE").getAsString();
            float x = jsonObject.get("X").getAsFloat();
            float y = jsonObject.get("Y").getAsFloat();
            try {
                physicalBusStopHashMap.get(busStopCode).X = x;
                physicalBusStopHashMap.get(busStopCode).Y = y;
            } catch (Exception e) {
                System.err.println("Stop " + busStopCode + " does not exist in file " + GlobalDataPath.BUS_STOP_LOCATIONS.toString());
            }
        }
    }

    private static void readSBSBusAndRouteData() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonArray jsonArray;
        JsonObject jsonObject;

        //Get all the busses
        jsonArray = gson.fromJson(new FileReader(GlobalDataPath.SBS_ROUTE.toString()), new TypeToken<JsonArray>() {
        }.getType());
        for (JsonElement jsonElement : jsonArray) {
            jsonObject = jsonElement.getAsJsonObject();

            //Get what's needed from the Json Object
            String svcNumber = jsonObject.get("SVC_NUM").getAsString();
            int dir = jsonObject.get("DIR").getAsInt();
            int route_seq = jsonObject.get("ROUTE_SEQ").getAsInt();
            String busStopCode = jsonObject.get("BS_CODE").getAsString();
            double distance = 0;
            if (!jsonObject.get("DISTANCE").getAsString().equals("-")) {
                distance = jsonObject.get("DISTANCE").getAsDouble();
            }

            //Create the bus. Bus object is a pointer.
            Bus bus;
            if (busHashMap.get(svcNumber) == null) {
                bus = new Bus();
                bus.SVC_NUM = svcNumber;
                busHashMap.put(bus.SVC_NUM, bus);
            } else {
                bus = busHashMap.get(svcNumber);
            }
            try {
                //Create the PhysicalBusStopForBus
                PhysicalBusStopForBus physicalBusStopForBus = new PhysicalBusStopForBus(physicalBusStopHashMap.get(busStopCode));
                physicalBusStopForBus.DIR = dir;
                physicalBusStopForBus.ROUTE_SEQ = route_seq;
                physicalBusStopForBus.DISTANCE = distance;
                physicalBusStopForBus.SVC_NUM = bus.SVC_NUM;

                //Add the PhysicalBusStopForBus to the Bus Object
                if (dir == 1) //                    bus.dirOne.put(physicalBusStopForBus.BUSCODE, physicalBusStopForBus);
                {
                    bus.dirOne.add(physicalBusStopForBus);
                } else //                    bus.dirTwo.put(physicalBusStopForBus.BUSCODE, physicalBusStopForBus);
                {
                    bus.dirTwo.add(physicalBusStopForBus);
                }

            } catch (Exception ex) {
                System.out.println("Stop " + busStopCode + " does not exist in the hashmap");
            }
        }
    }

    private static void readSMRTBusAndRouteData() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonArray jsonArray;
        JsonObject jsonObject;

        //Get all the busses
        jsonArray = gson.fromJson(new FileReader(GlobalDataPath.SMRT_ROUTE.toString()), new TypeToken<JsonArray>() {
        }.getType());
        for (JsonElement jsonElement : jsonArray) {
            jsonObject = jsonElement.getAsJsonObject();

            //Get what's needed from the Json Object
            String svcNumber = jsonObject.get("SVC_NUM").getAsString();
            int dir = jsonObject.get("DIR").getAsInt();
            int route_seq = jsonObject.get("ROUT_SEQ").getAsInt();
            String busStopCode = jsonObject.get("BS_CODE").getAsString();
            double distance = 0;
            if (!jsonObject.get("DISTANCE").getAsString().equals("-")) {
                distance = jsonObject.get("DISTANCE").getAsDouble();
            }

            //Create the bus. Bus object is a pointer.
            Bus bus;
            if (busHashMap.get(svcNumber) == null) {
                bus = new Bus();
                bus.SVC_NUM = svcNumber;
                busHashMap.put(bus.SVC_NUM, bus);
            } else {
                bus = busHashMap.get(svcNumber);
            }
            try {
                //Create the PhysicalBusStopForBus
                PhysicalBusStopForBus physicalBusStopForBus = new PhysicalBusStopForBus(physicalBusStopHashMap.get(busStopCode));
                physicalBusStopForBus.DIR = dir;
                physicalBusStopForBus.ROUTE_SEQ = route_seq;
                physicalBusStopForBus.DISTANCE = distance;
                physicalBusStopForBus.SVC_NUM = bus.SVC_NUM;

                //Add the PhysicalBusStopForBus to the Bus Object
                if (dir == 1) //                    bus.dirOne.put(physicalBusStopForBus.BUSCODE, physicalBusStopForBus);
                {
                    bus.dirOne.add(physicalBusStopForBus);
                } else //                    bus.dirTwo.put(physicalBusStopForBus.BUSCODE, physicalBusStopForBus);
                {
                    bus.dirTwo.add(physicalBusStopForBus);
                }

            } catch (Exception ex) {
                System.out.println("Stop " + busStopCode + " does not exist in the hashmap");
            }
        }
    }

    private static void setBussesAtBusStop() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonArray jsonArray;
        JsonObject jsonObject;

        //Get all the busses
        jsonArray = gson.fromJson(new FileReader(GlobalDataPath.SMRT_ROUTE.toString()), new TypeToken<JsonArray>() {
        }.getType());
        for (JsonElement jsonElement : jsonArray) {
            jsonObject = jsonElement.getAsJsonObject();

            //Get what's needed from the Json Object
            String svcNumber = jsonObject.get("SVC_NUM").getAsString();
            String busStopCode = jsonObject.get("BS_CODE").getAsString();

            //Create the bus. Bus object is a pointer.
            Bus bus = busHashMap.get(svcNumber);
            PhysicalBusStop physicalBusStop = physicalBusStopHashMap.get(busStopCode);
            physicalBusStop.BussesAtThisStop.add(bus);
        }

        jsonArray = gson.fromJson(new FileReader(GlobalDataPath.SBS_ROUTE.toString()), new TypeToken<JsonArray>() {
        }.getType());
        for (JsonElement jsonElement : jsonArray) {
            jsonObject = jsonElement.getAsJsonObject();

            //Get what's needed from the Json Object
            String svcNumber = jsonObject.get("SVC_NUM").getAsString();
            String busStopCode = jsonObject.get("BS_CODE").getAsString();

            //Create the bus. Bus object is a pointer.
            Bus bus = busHashMap.get(svcNumber);
            PhysicalBusStop physicalBusStop = physicalBusStopHashMap.get(busStopCode);
            physicalBusStop.BussesAtThisStop.add(bus);
        }
    }

    private static void setBusStopNeighbours() {
        boolean isFirst = true;
        String previousBusStopName = null;
        PhysicalBusStopForBus previousBusStopObject = null;
        for (Bus bus : busHashMap.values()) {
//            System.out.println("Setting neighbours for bus: " + bus.SVC_NUM);
//            System.out.println("Dir 1");
            for (PhysicalBusStopForBus entry : bus.dirOne) {
//                System.out.println("\t" + entry.BUSCODE + " - " + entry.ROUTE_SEQ);
                if (isFirst) {
                    previousBusStopName = entry.BUSCODE;
                    previousBusStopObject = entry;
                    isFirst = false;
                } else {
                    String currentBusStopName = entry.BUSCODE;
                    PhysicalBusStopForBus currentBusStopObject = entry;
                    if ((previousBusStopObject.ROUTE_SEQ + 1) == currentBusStopObject.ROUTE_SEQ) {
                        physicalBusStopHashMap.get(previousBusStopName).Neighbours.add(physicalBusStopHashMap.get(currentBusStopName));
                    }
                    previousBusStopName = currentBusStopName;
                    previousBusStopObject = currentBusStopObject;
                }
            }
//            System.out.println("Dir 2");
            for (PhysicalBusStopForBus entry : bus.dirTwo) {
//                System.out.println("\t" + entry.BUSCODE + " - " + entry.ROUTE_SEQ);
                if (isFirst) {
                    previousBusStopName = entry.BUSCODE;
                    isFirst = false;
                } else {
                    String currentBusStopName = entry.BUSCODE;
                    PhysicalBusStopForBus currentBusStopObject = entry;
                    if ((previousBusStopObject.ROUTE_SEQ + 1) == currentBusStopObject.ROUTE_SEQ) {
                        physicalBusStopHashMap.get(previousBusStopName).Neighbours.add(physicalBusStopHashMap.get(currentBusStopName));
                    }
                    previousBusStopName = currentBusStopName;
                    previousBusStopObject = currentBusStopObject;
                }
            }
        }
    }

    private static void readMrtData() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonObject jsonObject;
        JsonArray jsonArray;

        //Get the station names and codes
        try (BufferedReader br = new BufferedReader(new FileReader(GlobalDataPath.MRT_STATION_LIST.toString()))) {
            String line;
            while ((line = br.readLine()) != null) {
                try {
                    while (line.equals("(start)") || line.equals("(end)")) {
                        line = br.readLine();
                    }
                    String mrtCode = line.trim();
                    String mrtStationName = br.readLine();

                    mrtStationInfoMap.put(mrtCode, new MRTStation(mrtCode, mrtStationName));
//                    System.out.println(line);
//                    if (line.equals("CG1")) {
//                        System.out.println("cg");
//                    }
                } catch (NullPointerException e) {
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Get coordinates
        jsonObject = gson.fromJson(new FileReader(GlobalDataPath.MRT_STATIONS_LOCATIONS.toString()), new TypeToken<JsonObject>() {
        }.getType());
        jsonArray = jsonObject.get("root").getAsJsonObject().get("features").getAsJsonArray();
        for (JsonElement jsonElement : jsonArray) {
            jsonObject = jsonElement.getAsJsonObject();
            String mrtStationName = jsonObject.get("properties").getAsJsonObject().get("STN_NAM").getAsString();
            if (mrtStationName.contains("INTERCHANGE")) {
                mrtStationName = mrtStationName.replace(" INTERCHANGE", "");
            }
            float x = jsonObject.get("geometry").getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsFloat();
            float y = jsonObject.get("geometry").getAsJsonObject().get("coordinates").getAsJsonArray().get(1).getAsFloat();
            try {
                for (MRTStation mrtStation : mrtStationInfoMap.values()) {
                    if (mrtStation.MRTStationName.equalsIgnoreCase(mrtStationName)) {
                        mrtStation.X = x;
                        mrtStation.Y = y;
                    }
                }
            } catch (Exception e) {
                System.out.println("The station " + mrtStationName + " is missing. :(");
            }
        }
        //Create interchanges
        for (MRTStation mrtStation : mrtStationInfoMap.values()) {
            if (mrtStationInfoMap.get(mrtStation.MRTStationName) == null) {
                MRTStation newMrtStation = mrtStation;
//                System.out.println(newMrtStation.MRTCode);
                mrtStationInfoMap.put(newMrtStation.MRTStationName, newMrtStation);
                newMrtStation.setLineList();
            } else {
//                if (!mrtStationInfoMap.get(mrtStation.MRTStationName).MRTCode.containsAll(mrtStation.MRTCode))
                mrtStationInfoMap.get(mrtStation.MRTStationName).addMrtCode(mrtStation.MRTCode);
                mrtStationInfoMap.get(mrtStation.MRTStationName).setLineList();
            }
        }
    }

    private static void setMrtNeighbours() {
        for (MRTStation mrtStation : mrtStationInfoMap.values()) {
            for (String mrtCode : mrtStation.MRTCode) {
                String line = mrtCode.substring(0, 2);
                int number = Integer.parseInt(mrtCode.substring(2, mrtCode.length()));
                //get 1 up
                MRTStation mrtStationToAdd;
                if ((mrtStationToAdd = mrtStationInfoMap.get(line + (number + 1))) != null) {
                    mrtStation.Neighbours.add(mrtStationInfoMap.get(mrtStationToAdd.MRTStationName));
                } else if ((mrtStationToAdd = mrtStationInfoMap.get(line + (number + 2))) != null) {
                    mrtStation.Neighbours.add(mrtStationInfoMap.get(mrtStationToAdd.MRTStationName));
                }
                //get 1 down
                if ((mrtStationToAdd = mrtStationInfoMap.get(line + (number - 1))) != null) {
                    mrtStation.Neighbours.add(mrtStationInfoMap.get(mrtStationToAdd.MRTStationName));
                } else if ((mrtStationToAdd = mrtStationInfoMap.get(line + (number - 2))) != null) {
                    mrtStation.Neighbours.add(mrtStationInfoMap.get(mrtStationToAdd.MRTStationName));
                }
            }
        }
        MRTStation expo = mrtStationInfoMap.get("Expo");
        expo.Neighbours.add(mrtStationInfoMap.get("Tanah Merah"));
        expo = mrtStationInfoMap.get("CG1");
        expo.Neighbours.add(mrtStationInfoMap.get("Tanah Merah"));
        MRTStation tanahmerah = mrtStationInfoMap.get("Tanah Merah");
        tanahmerah.Neighbours.add(mrtStationInfoMap.get("Expo"));
        tanahmerah = mrtStationInfoMap.get("EW4");
        tanahmerah.Neighbours.add(mrtStationInfoMap.get("Expo"));
    }

    private static void readHawkerData() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonArray jsonArray;
        JsonObject jsonObject;

        //Get all hawker centers
        jsonObject = gson.fromJson(new FileReader(GlobalDataPath.HAWKER.toString()), new TypeToken<JsonObject>() {
        }.getType());
        jsonArray = jsonObject.get("root").getAsJsonObject().get("SrchResults").getAsJsonArray();
        for (JsonElement jsonElement : jsonArray) {
            jsonObject = jsonElement.getAsJsonObject();
            //Get what's needed from the Json object
            if (!jsonObject.has("FeatCount")) {
                String hawkerCenterName = jsonObject.get("NAME").getAsString();
                String xyCoords = jsonObject.get("XY").getAsString();
                double x = Double.parseDouble(xyCoords.split(",")[0]);
                double y = Double.parseDouble(xyCoords.split(",")[1]);
                hawkerCenterMap.put(hawkerCenterName, new HawkerCenter(hawkerCenterName, x, y));
            }

        }
    }

    private static void readHeritageData() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonArray jsonArray;
        JsonObject jsonObject;

        //Get all hawker centers
        jsonObject = gson.fromJson(new FileReader(GlobalDataPath.HERITAGE.toString()), new TypeToken<JsonObject>() {
        }.getType());
        jsonArray = jsonObject.get("root").getAsJsonObject().get("SrchResults").getAsJsonArray();
        for (JsonElement jsonElement : jsonArray) {
            jsonObject = jsonElement.getAsJsonObject();
            //Get what's needed from the Json object
            if (!jsonObject.has("FeatCount")) {
                String heritageSiteName = jsonObject.get("NAME").getAsString();
                String xyCoords = jsonObject.get("XY").getAsString();
                double x = Double.parseDouble(xyCoords.split(",")[0]);
                double y = Double.parseDouble(xyCoords.split(",")[1]);
                heritageSiteMap.put(heritageSiteName, new HeritageSite(heritageSiteName, x, y));
            }

        }
    }

    private static void readHistoricSiteData() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonArray jsonArray;
        JsonObject jsonObject;

        //Get all hawker centers
        jsonObject = gson.fromJson(new FileReader(GlobalDataPath.HISTORTIC_SITES.toString()), new TypeToken<JsonObject>() {
        }.getType());
        jsonArray = jsonObject.get("root").getAsJsonObject().get("SrchResults").getAsJsonArray();
        for (JsonElement jsonElement : jsonArray) {
            jsonObject = jsonElement.getAsJsonObject();
            //Get what's needed from the Json object
            if (!jsonObject.has("FeatCount")) {
                String historicSiteName = jsonObject.get("NAME").getAsString();
                String xyCoords = jsonObject.get("XY").getAsString();
                double x = Double.parseDouble(xyCoords.split(",")[0]);
                double y = Double.parseDouble(xyCoords.split(",")[1]);
                historicSiteMap.put(historicSiteName, new HistoricSite(historicSiteName, x, y));
            }

        }
    }

    private static void readHotelData() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonArray jsonArray;
        JsonObject jsonObject;

        //Get all hawker centers
        jsonObject = gson.fromJson(new FileReader(GlobalDataPath.HOTELS.toString()), new TypeToken<JsonObject>() {
        }.getType());
        jsonArray = jsonObject.get("root").getAsJsonObject().get("SrchResults").getAsJsonArray();
        for (JsonElement jsonElement : jsonArray) {
            jsonObject = jsonElement.getAsJsonObject();
            //Get what's needed from the Json object
            if (!jsonObject.has("FeatCount")) {
                String hotelName = jsonObject.get("NAME").getAsString();
                String xyCoords = jsonObject.get("XY").getAsString();
                double x = Double.parseDouble(xyCoords.split(",")[0]);
                double y = Double.parseDouble(xyCoords.split(",")[1]);
                hotelMap.put(hotelName, new Hotel(hotelName, x, y));
            }
        }
    }

    private static void readMuseumData() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonArray jsonArray;
        JsonObject jsonObject;

        //Get all hawker centers
        jsonObject = gson.fromJson(new FileReader(GlobalDataPath.MUSEUMS.toString()), new TypeToken<JsonObject>() {
        }.getType());
        jsonArray = jsonObject.get("root").getAsJsonObject().get("SrchResults").getAsJsonArray();
        for (JsonElement jsonElement : jsonArray) {
            jsonObject = jsonElement.getAsJsonObject();
            //Get what's needed from the Json object
            if (!jsonObject.has("FeatCount")) {
                String museumName = jsonObject.get("NAME").getAsString();
                String xyCoords = jsonObject.get("XY").getAsString();
                double x = Double.parseDouble(xyCoords.split(",")[0]);
                double y = Double.parseDouble(xyCoords.split(",")[1]);
                museumMap.put(museumName, new Museum(museumName, x, y));
            }
        }
    }

    private static void combine() {
        allLocationMap.putAll(hawkerCenterMap);
        allLocationMap.putAll(heritageSiteMap);
        allLocationMap.putAll(historicSiteMap);
        allLocationMap.putAll(hotelMap);
        allLocationMap.putAll(museumMap);
    }

}
